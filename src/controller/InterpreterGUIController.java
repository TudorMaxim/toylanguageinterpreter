package controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import model.PrgState;
import model.adt.MyDictionary;
import model.adt.MyHeap;
import model.adt.MyList;
import model.adt.MyStack;
import model.interfaces.*;
import model.utilities.Pair;
import repository.IRepo;
import repository.Repo;
import utilsTables.*;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class InterpreterGUIController {

    //HEAP TABLE
    @FXML private TableView<HeapTableView> heapTable;
    @FXML private TableColumn<HeapTableView, Integer> heapAddressColumn;
    @FXML private TableColumn<HeapTableView, Integer> heapValueColumn;

    //FILE TABLE
    @FXML private TableView<FileTableView> fileTable;
    @FXML private TableColumn<FileTableView, Integer> fileTableIdentifier;
    @FXML private TableColumn<FileTableView, String> fileTableName;

    //SYM TABLE
    @FXML private TableView<SymTableView> symTable;
    @FXML private TableColumn<SymTableView, String> symTableVarName;
    @FXML private TableColumn<SymTableView, Integer> symTableValue;

    @FXML private TableView <ProcTableView> procTableView;
    @FXML private TableColumn<ProcTableView, String> procTableSignatureView;
    @FXML private TableColumn<ProcTableView, String> procTableBodyView;

    @FXML private ListView<String> outList;
    @FXML private ListView<String> exeStack;
    @FXML private ListView<String> prgStateIdentifiers;
    @FXML private Button oneStepBTN;
    @FXML private Label noPrgStatesLabel;

    private Controller ctrl;

    @FXML
    public void initialize(){
        MyIStack<IStmt> stk = new MyStack<>();
        MyDictionary<String, Integer> sym = new MyDictionary<String, Integer>();
        MyIStack<MyDictionary<String, Integer>> symStk = new MyStack<>();
        symStk.push(sym);
        MyIList<Integer> out = new MyList<>();
        MyIDictionary<Integer, Pair<String, BufferedReader>> file = new MyDictionary<>();
        MyIHeap<Integer> heap = new MyHeap<>(new HashMap<>());
        MyIDictionary <String, Pair <ArrayList<String>, IStmt> > proc = new MyDictionary<>();
        PrgState programState = new PrgState(1, stk, symStk, out, file, heap, proc, SelectProgramController.statement);

        IRepo repo = new Repo(programState, "log.txt");
        ctrl = new Controller(repo);

        this.heapAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        this.heapValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        this.fileTableIdentifier.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.fileTableName.setCellValueFactory(new PropertyValueFactory<>("fileName"));

        this.symTableVarName.setCellValueFactory(new PropertyValueFactory<>("varName"));
        this.symTableValue.setCellValueFactory(new PropertyValueFactory<>("value"));

        this.procTableSignatureView.setCellValueFactory(new PropertyValueFactory<>("signature"));
        this.procTableBodyView.setCellValueFactory(new PropertyValueFactory<>("body"));

        setNoPrgState();
        setPrgStateIdentifiers();
        prgStateIdentifiers.getSelectionModel().select(0);
        setExeStack();
    }

    private void setNoPrgState(){
        Integer nr = ctrl.noPrgStates();
        noPrgStatesLabel.setText(String.valueOf(nr));
    }

    private void setPrgStateIdentifiers(){
        prgStateIdentifiers.setItems(ctrl.getPrgStatesID());
    }

    private PrgState getCurrentProgramState(){
        int index = prgStateIdentifiers.getSelectionModel().getSelectedIndex();
        if(index == -1) {
            index = 0;
        }
        return ctrl.getPrgStateByIndex(index);
    }

    private void setExeStack(){
        ObservableList<String> list = FXCollections.observableArrayList();
        PrgState programState = getCurrentProgramState();

        for(IStmt i : programState.getExeStack().getStack()) {
            list.add(i.toString());
        }
        Collections.reverse(list);
        exeStack.setItems(list);
    }

    private void setHeapTable(){
        ObservableList<HeapTableView> list = FXCollections.observableArrayList();
        PrgState programState = getCurrentProgramState();
        for(Integer key: programState.getHeap().getContent().keySet()) {
            list.add(new HeapTableView(key, programState.getHeap().readAddr(key)));
        }
        heapTable.setItems(list);
    }

    private void setFileTable(){
        ObservableList<FileTableView> list = FXCollections.observableArrayList();
        PrgState programState = getCurrentProgramState();
        for(Integer key: programState.getFileTable().getKeys()) {
            list.add(new FileTableView(key, programState.getFileTable().get(key).getFirst()));
        }
        fileTable.setItems(list);
    }

    private void setSymTable(){
        ObservableList<SymTableView> list = FXCollections.observableArrayList();
        PrgState programState = getCurrentProgramState();

        for(String key: programState.getSymTablesStack().top().getKeys()) {
            list.add(new SymTableView(key, programState.getSymTablesStack().top().get(key)));
        }
        symTable.setItems(list);
    }

    private void setOutList(){
        ObservableList<String> list = FXCollections.observableArrayList();
        PrgState programState = getCurrentProgramState();
        for(Integer i: programState.getOut().getList()) {
            list.add(i.toString());
        }
        outList.setItems(list);
    }

    private void setProcTable() {
        ObservableList<ProcTableView> list = FXCollections.observableArrayList();
        PrgState programState = getCurrentProgramState();

        for(String key: programState.getProcTable().getKeys()) {
            list.add(new ProcTableView(key + programState.getProcTable().get(key).getFirst().toString(), programState.getProcTable().get(key).getSecond().toString()));
        }
        procTableView.setItems(list);
    }


    private void setAll(){
        setNoPrgState();
        setPrgStateIdentifiers();
        setExeStack();
        setHeapTable();
        setFileTable();
        setSymTable();
        setOutList();
        setProcTable();
    }
    private void createAlertFromException(Exception ex) {
        System.out.println(ex);
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Exception Dialog");
        alert.setHeaderText("Program State Exception Dialog");
        alert.setContentText("There was an exception:\n" + ex.getMessage());
        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();
    }

    public void executeOneStep(ActionEvent ae){
        try {
            if (ctrl.oneStepGUI()) {
                setAll();
            } else {
                setNoPrgState();
                setPrgStateIdentifiers();
            }
        } catch (RuntimeException e){
            Node source = (Node) ae.getSource();
            Stage theStage = (Stage) source.getScene().getWindow();
            createAlertFromException(e);
            theStage.close();
        }
    }
    public void mouseClickPrgStateIdentifiers(){
        if(ctrl.noPrgStates() > 0)
            setAll();
    }
}