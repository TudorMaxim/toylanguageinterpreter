package controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.PrgState;
import model.adt.MyDictionary;
import model.adt.MyHeap;
import model.adt.MyList;
import model.adt.MyStack;
import model.exam.Call;
import model.exam.Procedure;
import model.exam.Sleep;
import model.expression.ArithmExpr;
import model.expression.BooleanExpr;
import model.expression.ConstExpr;
import model.expression.VarExpr;
import model.interfaces.*;
import model.statement.basic.*;
import model.statement.fileManagement.closeRFile;
import model.statement.fileManagement.openRFile;
import model.statement.fileManagement.readFile;
import model.statement.heapManagement.New;
import model.statement.heapManagement.ReadHeap;
import model.statement.heapManagement.WriteHeap;
import model.statement.threading.ForkStmt;
import model.utilities.Pair;
import repository.IRepo;
import repository.Repo;
import views.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SelectProgramController {
    @FXML
    private ListView<String> listView;
    @FXML
    private Button runProgramBtn;
    public static IStmt statement;
    private List<IStmt> stmtList = new ArrayList<IStmt>();

    @FXML
    public void initialize() {
//        IStmt ex1 = new CompStmt(
//                new AssignStmt("v", new ConstExpr(2)),
//                new PrintStmt(new VarExpr("v"))
//        );
//
//        IStmt ex2 = new CompStmt(
//                new AssignStmt("a",
//                        new ArithmExpr(
//                                new ConstExpr(2),
//                                '+',
//                                new ArithmExpr(new ConstExpr(3), '*', new ConstExpr(5))
//                        )
//                ),
//                new CompStmt(new AssignStmt("b",
//                        new ArithmExpr(new VarExpr("a"),
//                                '+',
//                                new ConstExpr(1))),
//                        new PrintStmt(new VarExpr("b"))
//                )
//        );
//
//        IStmt ex3 = new CompStmt(
//                new AssignStmt("a",
//                        new ArithmExpr(new ConstExpr(2), '-',
//                                new ConstExpr(2)
//                        )
//                ),
//                new CompStmt(
//                        new CondStmt(new VarExpr("a"),
//                                new AssignStmt("v", new ConstExpr(2)),
//                                new AssignStmt("v", new ConstExpr(3))),
//                        new PrintStmt(new VarExpr("v")))
//        );
//
//        IStmt errorNotDefined = new PrintStmt(new VarExpr("X"));
//
//        IStmt errorDivByZero = new PrintStmt(new ArithmExpr(new ConstExpr(5), '/', new ConstExpr(0)));
//
//        /*
//            openRFile(var_f,"test.in");
//            readFile(var_f,var_c);print(var_c);
//            (if var_c then readFile(var_f,var_c);print(var_c)
//            else print(0));
//            closeRFile(var_f)
//         */
//        IStmt ex6 = new CompStmt(
//                new openRFile("var_f", "test.in"),
//                new CompStmt(
//                        new readFile( new VarExpr("var_f"), "var_c"),
//                        new CompStmt(
//                                new PrintStmt(new VarExpr("var_c")),
//                                new CompStmt(
//                                        new CondStmt(
//                                                new VarExpr("var_c"),
//                                                new CompStmt(
//                                                        new readFile(new VarExpr("var_f"), "var_c"),
//                                                        new PrintStmt(new VarExpr("var_c"))
//                                                ),
//                                                new PrintStmt(new ConstExpr(0))
//                                        ),
//                                        new closeRFile(new VarExpr("var_f"))
//                                )
//                        )
//                )
//        );
//
//        IStmt ex7 = new CompStmt(
//                new openRFile("var_f", "test.in"),
//                new CompStmt(
//                        new readFile( new VarExpr("var_f + 2"), "var_c"),
//                        new CompStmt(
//                                new PrintStmt(new VarExpr("var_c")),
//                                new CompStmt(
//                                        new CondStmt(
//                                                new VarExpr("var_c"),
//                                                new CompStmt(
//                                                        new readFile(new VarExpr("var_f"), "var_c"),
//                                                        new PrintStmt(new VarExpr("var_c"))
//                                                ),
//                                                new PrintStmt(new ConstExpr(0))
//                                        ),
//                                        new closeRFile(new VarExpr("var_f"))
//                                )
//                        )
//                )
//        );
//        //A4
//        //v=10; new(v,20); new(a,22); print(v)
//        IStmt ex8 = new CompStmt(
//                new AssignStmt("v", new ConstExpr(10)),
//                new CompStmt(
//                        new New("v", new ConstExpr(20)),
//                        new CompStmt(
//                                new New("a", new ConstExpr(22)),
//                                new PrintStmt(new VarExpr("v"))
//                        )
//                )
//        );
//
//        //x=1; new(v,20); new(a,22); print(x); print(v);
//        IStmt ex9 = new CompStmt(
//                new AssignStmt("x", new ConstExpr(1)),
//                new CompStmt(
//                        new New("v", new ConstExpr(20)),
//                        new CompStmt(
//                                new New("a", new ConstExpr(22)),
//                                new CompStmt(
//                                        new PrintStmt(new VarExpr("x")),
//                                        new PrintStmt(new VarExpr("v"))
//                                )
//                        )
//                )
//        );
//
//        //v=10;new(v,20);new(a,22);print(100+rH(v));print(100+rH(a))
//        IStmt ex10 = new CompStmt(
//                new AssignStmt("v", new ConstExpr(10)),
//                new CompStmt(
//                        new New("v", new ConstExpr(20)),
//                        new CompStmt(
//                                new New("a", new ConstExpr(22)),
//                                new CompStmt(
//                                        new PrintStmt(
//                                                new ArithmExpr(
//                                                        new ConstExpr(100),
//                                                        '+',
//                                                        new ReadHeap("v")
//                                                )
//                                        ),
//                                        new PrintStmt(
//                                                new ArithmExpr(
//                                                        new ConstExpr(100),
//                                                        '+',
//                                                        new ReadHeap("a")
//                                                )
//                                        )
//                                )
//                        )
//                )
//        );
//
//        // v=10;new(v,20);new(a,22);wH(a,30);print(a);print(rH(a))
//        IStmt ex11 = new CompStmt(
//                new AssignStmt("v", new ConstExpr(10)),
//                new CompStmt(
//                        new New("v", new ConstExpr(20)),
//                        new CompStmt(
//                                new New("a", new ConstExpr(22)),
//                                new CompStmt(
//                                        new WriteHeap(
//                                                "a",
//                                                new ConstExpr(30)
//                                        ),
//                                        new CompStmt(
//                                                new PrintStmt(new VarExpr("a")),
//                                                new PrintStmt(new ReadHeap("a"))
//                                        )
//                                )
//                        )
//                )
//        );
//        //v=10;new(v,20);new(a,22);wH(a,30);print(a);print(rH(a)); a=0
//        IStmt ex12 = new CompStmt(
//                new AssignStmt("v", new ConstExpr(10)),
//                new CompStmt(
//                        new New("v", new ConstExpr(20)),
//                        new CompStmt(
//                                new New("a", new ConstExpr(22)),
//                                new CompStmt(
//                                        new WriteHeap(
//                                                "a",
//                                                new ConstExpr(30)
//                                        ),
//                                        new CompStmt(
//                                                new PrintStmt(new VarExpr("a")),
//                                                new CompStmt(
//                                                        new PrintStmt(new ReadHeap("a")),
//                                                        new AssignStmt("a", new ConstExpr(0))
//                                                )
//                                        )
//                                )
//                        )
//                )
//        );
//        //v=6; (while (v-4) print(v);v=v-1);print(v)
//        IStmt ex13 = new CompStmt(
//                new AssignStmt("v", new ConstExpr(6)),
//                new CompStmt(
//                        new WhileStmt(
//                                new BooleanExpr(
//                                        new ArithmExpr(new VarExpr("v"), '-', new ConstExpr(4)),
//                                        "!=",
//                                        new ConstExpr(0)
//                                ),
//                                new CompStmt(
//                                        new PrintStmt(new VarExpr("v")),
//                                        new AssignStmt(
//                                                "v",
//                                                new ArithmExpr(new VarExpr("v"), '-', new ConstExpr(1))
//                                        )
//                                )
//                        ),
//                        new PrintStmt(new VarExpr("v"))
//                )
//        );

        IStmt ex14 = new CompStmt(
                new CompStmt(
                        new AssignStmt("v", new ConstExpr(10)),
                        new New("a", new ConstExpr(22))
                ),
                new CompStmt(
                        new ForkStmt(
                                new CompStmt(
                                        new WriteHeap("a", new ConstExpr(30)),
                                        new CompStmt(
                                                new AssignStmt("v", new ConstExpr(32)),
                                                new CompStmt(
                                                        new PrintStmt(new VarExpr("v")),
                                                        new PrintStmt(new ReadHeap("a"))
                                                )
                                        )
                                )
                        ),
                        new CompStmt(
                                new PrintStmt(new VarExpr("v")),
                                new PrintStmt(new ReadHeap("a"))
                        )
                )
        );
//        v=10;
//        (fork(v=v-1;v=v-1;print(v)); sleep(10);print(v*10)
        IStmt simpleProblem = new CompStmt(
                new AssignStmt("v", new ConstExpr(10)),
                new CompStmt(
                        new ForkStmt(
                                new CompStmt(
                                       new AssignStmt("v", new ArithmExpr(new VarExpr("v"), '-', new ConstExpr(1))),
                                        new CompStmt(
                                                new AssignStmt("v", new ArithmExpr(new VarExpr("v"), '-', new ConstExpr(1))),
                                               new PrintStmt(new VarExpr("v"))
                                        )
                                )
                        ),
                        new CompStmt(
                                new Sleep(10),
                                new PrintStmt(new ArithmExpr(new VarExpr("v"), '*' , new ConstExpr(10)))
                        )
                )
        );

        ArrayList<String> params = new ArrayList<>();
        params.add("a");
        params.add("b");

        IStmt procedure1 = new Procedure("sum", params, new CompStmt(
                        new AssignStmt("v", new ArithmExpr(new VarExpr("a"), '+', new VarExpr("b"))),
                        new PrintStmt(new VarExpr("v"))
        ));

        IStmt procedure2 = new Procedure("product", params, new CompStmt(
                new AssignStmt("v", new ArithmExpr(new VarExpr("a"), '*', new VarExpr("b"))),
                new PrintStmt(new VarExpr("v"))
        ));

        ArrayList <IExpression> params1 = new ArrayList<>();
        ArrayList <IExpression> params2 = new ArrayList<>();
        params1.add(
                new ArithmExpr(new VarExpr("v"), '*', new ConstExpr(10)));
        params1.add(
                new VarExpr("w"));

        params2.add(new VarExpr("v"));
        params2.add(new VarExpr("w"));


        IStmt exam = new CompStmt(
                        new CompStmt(
                                procedure1,
                                procedure2
                        ),
                        new CompStmt(
                                new AssignStmt("v", new ConstExpr(2)),
                                new CompStmt(
                                        new AssignStmt("w", new ConstExpr(5)),
                                        new CompStmt(
                                                new Call("sum", params1),
                                                new CompStmt(
                                                        new PrintStmt(new VarExpr("v")),
                                                        new ForkStmt(
                                                                new CompStmt(
                                                                        new Call("product", params2),
                                                                        new ForkStmt(
                                                                                new Call("sum", params2)
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                );
//        stmtList.add(ex1);
//        stmtList.add(ex2);
//        stmtList.add(ex3);
//        stmtList.add(errorDivByZero);
//        stmtList.add(errorNotDefined);
//        stmtList.add(ex6);
//        stmtList.add(ex7);
//        stmtList.add(ex8);
//        stmtList.add(ex9);
//        stmtList.add(ex10);
//        stmtList.add(ex11);
//        stmtList.add(ex12);
//        stmtList.add(ex13);
        stmtList.add(ex14);
        stmtList.add(simpleProblem);
        stmtList.add(exam);
        ObservableList<String> list = FXCollections.observableArrayList();
        for(IStmt i : stmtList) {
            list.add(i.toString());
        }
        listView.setItems(list);
        listView.getSelectionModel().selectIndices(0);
    }

    @FXML
    public void buttonRun() throws IOException {
        statement = stmtList.get(listView.getSelectionModel().getSelectedIndex());
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(Main.class.getResource("InterpreterGUI.fxml"));
        stage.setTitle("Running Program");
        stage.setScene(new Scene(root, 800, 600));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();

    }

}