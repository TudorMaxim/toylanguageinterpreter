package controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.PrgState;
import model.exceptions.ControllerException;
import model.interfaces.MyIDictionary;
import model.utilities.Pair;
import repository.IRepo;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Controller {
    private IRepo repo;
    private ExecutorService executor;

    public Controller(IRepo repo) {
        this.repo = repo;
        executor =  Executors.newFixedThreadPool(2);
    }

    public int noPrgStates(){
        return repo.getList().size();
    }

    public PrgState getPrgStateByIndex(int index){
        return  repo.getList().get(index);
    }

    public ObservableList<String> getPrgStatesID(){
        ObservableList<String> list = FXCollections.observableArrayList();
        for(PrgState i : repo.getList()) {
            list.add(String.valueOf(i.getId()));
        }
        return list;
    }

    private Map<Integer,Integer> conservativeGarbageCollector(Collection<Integer> symTableValues, Map<Integer,Integer> heap){
        return heap.entrySet().stream()
                .filter(e->symTableValues.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private List <PrgState> removeCompletedPrg(List<PrgState> inPrgList) {
        return inPrgList.stream().filter(PrgState::isNotCompleted).collect(Collectors.toList());
    }

    private void oneStepForAll(List <PrgState> programsList) {
        List<Callable<PrgState>> callList = programsList.stream()
                .map((PrgState p) -> (Callable<PrgState>)(() -> {
                            return p.oneStep();
                        })
                ).collect(Collectors.toList());

        List<PrgState> newProgramsList = null;
        try {
            newProgramsList = executor.invokeAll(callList).stream().map(future -> {
                        try {
                            return future.get();
                        } catch (InterruptedException e) {
                            throw new ControllerException(e.getMessage());
                        } catch (ExecutionException e) {
                            throw new ControllerException(e.getMessage());
                        }
                    }
            ).filter(Objects::nonNull).collect(Collectors.toList());
        }catch (InterruptedException ex) {
            throw new ControllerException(ex.getMessage());
        }
         programsList.addAll(newProgramsList);
        //the heap is the same for all programmes and we will use the merged sym table
//        MyIDictionary<String, Integer> mergedSymTable = repo.mergeSymTables();
//        programsList.get(0).getHeap().setContent(
//                conservativeGarbageCollector(
//                        mergedSymTable.getContent().values(),
//                        programsList.get(0).getHeap().getContent())
//        );
        programsList.forEach(prg -> {
            try {
                repo.logPrgStateExec(prg);
            }catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        });
        repo.setList(programsList);
    }

    public boolean oneStepGUI (){
        List<PrgState> prgList = removeCompletedPrg(repo.getList());
        if(prgList.size() > 0){
            oneStepForAll(prgList);
            prgList = removeCompletedPrg(repo.getList());
            return true;
        }
        executor.shutdownNow();
        repo.setList(prgList);
        return false;
    }

    public void allSteps() {
        executor = Executors.newFixedThreadPool(2);
        List<PrgState> programmesList = removeCompletedPrg(repo.getList());
        while (programmesList.size() > 0) {
            oneStepForAll(programmesList);
            programmesList = removeCompletedPrg(repo.getList());
        }
        executor.shutdown();
        //here is at least one prg in the repo
        repo.getList().get(0).getFileTable().getValues().stream().map(Pair::getSecond).forEach(bufferedReader -> {
                    try {
                        bufferedReader.close();
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
        );
        repo.setList(programmesList);
    }
}
