package model.exam;

import model.PrgState;
import model.adt.MyDictionary;
import model.interfaces.IExpression;
import model.interfaces.IStmt;
import model.interfaces.MyIDictionary;

import java.security.spec.ECField;
import java.util.ArrayList;

public class Call implements IStmt {
    private ArrayList <IExpression> params;
    private String procName;

    public Call(String procName, ArrayList <IExpression> params) {
        this.procName = procName;
        this.params = params;
    }

    public PrgState execute(PrgState state) throws Exception {
        if (state.getProcTable().get(procName) == null) {
           throw  new Exception("Error, Procedure does not exist");
        }
        if (state.getProcTable().get(procName).getFirst().size() != params.size()) {
            throw  new Exception("Error, invalid number of parameters");
        }
        MyDictionary <String, Integer> symTable = new MyDictionary<>();
        ArrayList <String> paramNames = state.getProcTable().get(procName).getFirst();
        for (int i = 0; i < paramNames.size(); i++) {
            Integer res = this.params.get(i).eval(state.getSymTablesStack().top(), state.getHeap());
            symTable.put(paramNames.get(i), res);
        }
        state.getSymTablesStack().push(symTable);
        state.getExeStack().push(new Return());
        state.getExeStack().push(state.getProcTable().get(procName).getSecond());
        return null;
    }
    public String toString() {
        return  "call " + procName + "(" + params.toString() + ")";
    }
}
