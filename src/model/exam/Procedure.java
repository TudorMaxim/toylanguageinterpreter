package model.exam;
import model.PrgState;
import model.interfaces.IExpression;
import model.interfaces.IStmt;
import model.utilities.Pair;

import java.util.ArrayList;

public class Procedure implements IStmt {
    private String procName;
    private ArrayList<String> params;
    private IStmt body;

    public Procedure(String procName, ArrayList<String> params, IStmt body) {
        this.procName = procName;
        this.params = params;
        this.body = body;
    }

    public PrgState execute(PrgState state) throws Exception{
        if (state.getProcTable().get(this.procName) !=  null) {
            throw new Exception("Error: A procedure with the same name already exists!");
        }
        state.getProcTable().put(procName, new Pair< ArrayList<String>, IStmt>(params, body));
        return null;
    }

    public String toString() {
        return "procedure(" + params.toString() + ") { " + body.toString() + "} ";
    }
}
