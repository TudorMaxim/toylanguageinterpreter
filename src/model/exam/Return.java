package model.exam;

import model.PrgState;
import model.interfaces.IStmt;

public class Return implements IStmt {
    public Return() {}

    public String toString() {
        return "return";
    }

    public PrgState execute(PrgState state) {
        state.getSymTablesStack().pop();
        return null;
    }
}
