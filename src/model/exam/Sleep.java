package model.exam;
import model.PrgState;
import model.interfaces.IStmt;

public class Sleep implements IStmt {
    private Integer cnt;

    public Sleep(Integer cnt) {
        this.cnt = cnt;
    }

    public PrgState execute(PrgState state) {
        if (this.cnt > 0) {
            state.getExeStack().push(new Sleep(cnt - 1));
        }
        return null;
    }
    public String toString() {
        return "sleep(" + cnt.toString() + ")";
    }
}
