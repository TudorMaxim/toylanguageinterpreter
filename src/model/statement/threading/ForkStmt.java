package model.statement.threading;

import model.PrgState;
import model.adt.MyDictionary;
import model.adt.MyStack;
import model.interfaces.IStmt;
import model.interfaces.MyIDictionary;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class ForkStmt implements IStmt {
    private IStmt state;
    private static Integer id = 1;
    public ForkStmt (IStmt state) {
        this.state = state;
    }
    public PrgState execute(PrgState prg) throws Exception {
        Stack <MyDictionary<String, Integer>> newStk = new Stack<>();
        ArrayList <MyDictionary<String, Integer>> aux = new ArrayList<>();
        Stack <MyDictionary <String, Integer>> stk = prg.getSymTablesStack().getStack();
        while (!stk.isEmpty()) {
            aux.add(stk.peek());
            stk.pop();
        }
        for (MyDictionary<String, Integer> symTable : aux) {
            stk.push(symTable);
            newStk.push(symTable.duplicate());
        };

        return new PrgState(
                ++id,
                new MyStack<IStmt>(),
                new MyStack<MyDictionary<String, Integer>>(newStk),//prg.getSymTable().duplicate(), // duplicate symtable stack
                prg.getOut(),
                prg.getFileTable(),
                prg.getHeap(),
                prg.getProcTable(),
                state
        );
    }

    public String toString() {
        return "fork(" + state.toString() + ")";
    }
}
