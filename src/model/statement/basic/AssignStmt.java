package model.statement.basic;

import model.PrgState;
import model.interfaces.IExpression;
import model.interfaces.IStmt;
import model.interfaces.MyIDictionary;

public class AssignStmt implements IStmt {
    String name;
    IExpression expr;

    public AssignStmt(String name, IExpression expr) {
        this.name = name;
        this.expr = expr;
    }

    public PrgState execute(PrgState state) throws Exception {
        MyIDictionary<String, Integer> symTable = state.getSymTablesStack().top();
        Integer ans = this.expr.eval(symTable, state.getHeap());
        symTable.put(this.name, ans);
        return null;
    }

    public String toString() {
        return name.toString() + " = " + expr.toString() + "; ";
    }
}
