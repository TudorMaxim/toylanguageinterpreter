package model.interfaces;
import java.io.Serializable;
import java.util.Stack;

public interface MyIStack<T> extends Serializable {
    void push(T elem);
    T pop();
    T top();
    boolean empty();
    Stack<T> getStack();
    //MyIStack <T> duplicate();
}
