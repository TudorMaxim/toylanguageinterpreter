package model.interfaces;

import java.io.Serializable;

public interface IExpression extends Serializable {
    int eval(MyIDictionary<String, Integer> symTable, MyIHeap<Integer> heap) throws Exception;
    String toString();
    // IExpression duplicate();
}
