package model.interfaces;
import model.PrgState;
import java.io.Serializable;

public interface IStmt extends Serializable {
    String toString();
    //IStmt duplicate();
    PrgState execute(PrgState prg) throws Exception;
}
