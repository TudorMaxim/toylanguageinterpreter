package model.interfaces;

import java.io.Serializable;
import java.util.ArrayList;

public interface MyIList<T> extends Serializable {
    T get(int index);
    int getIndex(T val);
    void add(T val);
    T removeByIndex(int index);
    boolean remove(T val);
    T find(T val);
    boolean contains(T val);
    int size();
    void clear();
    ArrayList <T> getList();
}
