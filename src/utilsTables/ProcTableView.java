package utilsTables;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ProcTableView {
    private SimpleStringProperty signature;
    private SimpleStringProperty body;

    public ProcTableView(String signature, String body) {
        this.signature =  new SimpleStringProperty(signature);
        this.body =  new SimpleStringProperty(body);
    }

    public String getSignature() {
        return signature.get();
    }

    public SimpleStringProperty signatureProperty() {
        return signature;
    }

    public String getBody() {
        return body.get();
    }

    public SimpleStringProperty bodyProperty() {
        return body;
    }

    public void setSignature(String varName) {
        this.signature.set(varName);
    }

    public void setBody(String value) {
        this.body.set(value);
    }
}