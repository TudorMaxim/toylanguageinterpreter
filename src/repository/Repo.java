package repository;
import model.PrgState;
import model.adt.MyDictionary;
import model.interfaces.MyIDictionary;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Repo implements IRepo {
    private String logFile;
    private List<PrgState> prgStates;
    private PrintWriter printWriter;
    private boolean firstTime;

    public Repo(PrgState prg, String logFile) {
        this.prgStates = new ArrayList<PrgState>();
        this.prgStates.add(prg);
        this.firstTime = true;
        this.printWriter = null;
        this.logFile = logFile;
    }

    public Repo(ArrayList<PrgState> prgStates, String logFile) {
        this.prgStates = prgStates;
        this.firstTime = true;
        this.printWriter = null;
        this.logFile = logFile;
    }

    public List<PrgState> getList() {
        return this.prgStates;
    }

    public void setList(List<PrgState> list) {
        this.prgStates = list;
    }

    public void add(PrgState prg) {
        prgStates.add(prg);
    }

    @Override
    public void logPrgStateExec(PrgState state) throws Exception {
        if(firstTime) {
            /// first, clear the content
            try {
                PrintWriter writer = new PrintWriter(new File(logFile));
                writer.print("");
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            firstTime = false;
        }
        this.printWriter = new PrintWriter(new FileWriter(logFile, true));
        this.printWriter.append(state.toString());
        this.printWriter.flush();
        this.printWriter.close();
    }

    public MyIDictionary<String,Integer> mergeSymTables() {
        Map<String, Integer> mergedSymTable = new HashMap<String, Integer>();
        this.prgStates.forEach(program -> {
            mergedSymTable.putAll(program.getSymTablesStack().top().getContent());
        });
        return new MyDictionary<String, Integer>(mergedSymTable);
    }

    @Override
    public void serialize() {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream("serialize.txt"));
            out.write(prgStates.size());
            for(PrgState t : prgStates)
                out.writeObject(t);
        } catch (IOException e) {
            System.out.println("IOError\nError: " + e.toString());
        } finally {
            if(out != null) {
                try {
                    out.close();
                } catch(IOException e) {
                    System.out.println("IOError\nError: " + e.toString());
                }
            }
        }
    }

    @Override
    public void deserialize() {
        ObjectInputStream in = null;
        PrgState state = null;
        try {
            in = new ObjectInputStream(new FileInputStream("serialize.txt"));
            int size = in.read();
            prgStates.clear();
            for(int i = 0; i < size; ++ i)
                prgStates.add((PrgState) in.readObject());
        } catch(IOException e) {
            System.out.println("IOError\nError: " + e.toString());
        } catch(ClassNotFoundException e) {
            System.out.println("ClassNotFoundException\nError: " + e.toString());
        }
        finally {
            if(in != null) {
                try {
                    in.close();
                } catch(IOException e) {
                    System.out.println("IOError\nError: " + e.toString());
                }
            }
        }
    }
}
